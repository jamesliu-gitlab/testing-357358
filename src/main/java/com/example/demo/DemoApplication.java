package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.SneakyThrows;

@SpringBootApplication
public class DemoApplication {

	@GetMapping("/hello-world")
	@ResponseBody
	@SneakyThrows
	public String sayHello(@RequestParam(name="name", required=false, defaultValue="Stranger") String name) {
	  return "lol";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
